import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList, Button } from 'react-native';
import ValueInput from './components/ValueInput';
import ValueItem from './components/ValueItem';

export default function App() {
  const [NewValue, setNewValue] = useState([]);
  const [IsAddMode, setIsAddMode] = useState(false);
  
  const addValueHandler = ValueTitle => {
    setNewValue(currentValue => [
      ...currentValue, {
        id: Math.random().toString(), value : ValueTitle
      }
    ]);
    setIsAddMode(false);
  };

  const CancelInput = () => {
    setIsAddMode(false);
  };

  const removeValueHandler = ValueID => {
    setNewValue(currentValue => {
      return currentValue.filter((values) => 
      values.id !== ValueID)
    });
  };

  return (
    <View style={styles.screen}>
      <Button
        title= "Add new list item"
        onPress= {() => setIsAddMode(true)}/>
      <ValueInput
        visible= {IsAddMode}
        onAddValue= {addValueHandler}
        onCancel={CancelInput}/>
      <FlatList
        data={NewValue} renderItem={itemData => 
        <ValueItem
          id={itemData.item.id}
          onDelete= {removeValueHandler}
          title= {itemData.item.value}/>}/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 30,
  },
});
