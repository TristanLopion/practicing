import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

const ValueItem = props => {
    return (
        <TouchableOpacity
        onPress= {props.onDelete.bind(this, props.id)}>
            <View style={styles.ListItem}>
                <Text>
                    {props.title}
                </Text>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    ListItem: {
        padding: 10,
        marginVertical: 10,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: 'lightgrey'
    },
});

export default ValueItem;